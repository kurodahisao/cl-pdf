;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package asdf)

(defsystem :cl-pdf-patch
  :name "cl-pdf-patch"
  :author "KURODA Hisao <littlelisper@gmail.com>"
  :maintainer "KURODA Hisao <littlelisper@gmail.com>"
  :description "CL-PDF Patch"
  :long-description "CL-PDF Patch"
  :components ((:file "cl-pdf-patch" :depends-on () :encoding :cp932))
  :depends-on (:asdf-encodings :cl-pdf))
