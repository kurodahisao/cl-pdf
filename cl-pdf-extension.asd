;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package asdf)

(defpackage "PDF"
  (:export "LOAD-IPA-FONT"
           "REMOVE-EMBEDDED-FONTS"
           "COMPRESS-PDF-FONTS"
           "PDF2TEXT"
           "PDF-ADJUST-OYAMOJI"
           "PDF-GUESS-LINE-BOTTOM")
  #+allegro
  (:import-from "EXCL" "STRING-TO-OCTETS" "OCTETS-TO-STRING" "WITH-INPUT-FROM-BUFFER" "WITH-OUTPUT-TO-BUFFER" "READ-VECTOR" "WRITE-VECTOR" "UNREAD-BYTE" "MATCH-REGEXP" "RUN-SHELL-COMMAND")
  #+allegro
  (:import-from "SYS" "MAKE-TEMP-FILE-NAME")
  #-allegro
  (:import-from "ASDF/BACKWARD-INTERFACE" "RUN-SHELL-COMMAND")
  #+sbcl
  (:import-from "SB-EXT" "STRING-TO-OCTETS" "OCTETS-TO-STRING"))

(defsystem :cl-pdf-extension
  :name "cl-pdf-extension"
  :author "KURODA Hisao <littlelisper@gmail.com>"
  :maintainer "KURODA Hisao <littlelisper@gmail.com>"
  :description "CL-PDF Extension"
  :long-description "CL-PDF Extension"
  :components ((:file "cl-pdf-extension" :depends-on () :encoding :cp932))
  :depends-on (:asdf-encodings :cl-pdf :cl-pdf-patch :babel :cl-ppcre :flexi-streams :deflate :salza2))
