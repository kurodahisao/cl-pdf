;;; -*- coding:cp932 -*-

(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload "cl-pdf-parser"))

(in-package "PDF")

;;; +external-format+
(defconstant +external-format+
  #-(or sbcl lispworks clisp allegro ccl abcl ecl) :default
  #+abcl '(:iso-8859-1 :eol-style :lf)
  #+ecl '(:latin-1 :lf)
  #+ccl :latin1
  #+sbcl :latin-1
  #+allegro :octets     ; :default が multi-byte-char に設定されてゐると error になるため常にoctets
  #+lispworks '(:latin-1 :eol-style :lf)
  #+clisp (ext:make-encoding :charset 'charset:iso-8859-1 :line-terminator :unix))

(defun read-pdf-file (file)             ; original と全く同じ定義だが
                                        ; +external-format+ が constant なので compile 仕直さないと反映されない
  (let ((*indirect-objects* (make-hash-table :test #'equal))
	(*document* (make-instance 'document :empty t)))
    (with-open-file (*pdf-input-stream* file
		     :direction :input
		     :external-format +external-format+)
      (read-pdf))
    *document*))

(defun find-cross-reference-start ()
  "file-length が +xref-search-size+ よりも小さい時に對應"
  (let* ((file-length (file-length *pdf-input-stream*))
         (xref-search-size (min +xref-search-size+ file-length))
         (buffer (make-string xref-search-size)))
    (file-position *pdf-input-stream* (- file-length xref-search-size))
    (read-sequence buffer *pdf-input-stream*)
    (let ((position (search "startxref" buffer)))
      (unless position
        (error 'pdf-parse-error :stream *pdf-input-stream*
               :message "Can't find start address of cross-reference table."))
      (parse-integer buffer :start (+ position 10) :junk-allowed t))))

(defun hex-digit-char-p (char)          ; 小文字も有効
  (find char "0123456789ABCDEFabcdef"))

(defun parse-hex (digit-1 digit-2)      ; not exist in cl-pdf-20210531-git
  (flet ((parse-digit (digit)
           (- (char-code digit)
              (if (char<= #\0 digit #\9) #.(char-code #\0) #.(- (char-code #\A) 10)))))
    (+ (* 16 (parse-digit (char-upcase digit-1)))
       (parse-digit (char-upcase digit-2)))))

(defun read-name ()
  (with-output-to-string (s)
    (write-char #\/ s)
    (loop (let ((char (read-char *pdf-input-stream* nil nil)))
	    (cond ((null char) (return))
		  ((eql char #\#)
		   (let ((digit-1 (read-char *pdf-input-stream*))
			 (digit-2 (read-char *pdf-input-stream*)))
		     (unless (and (hex-digit-char-p digit-1)
				  (hex-digit-char-p digit-2))
		       (error 'pdf-parse-error
			      :stream *pdf-input-stream*
			      :message "Illegal hexadecimal escape sequence in PDF name."))
		     (write-char (code-char (parse-hex digit-1 digit-2))
				 s)))
		  ((name-char-p char)
		   (write-char char s))
		  ((extended-ascii-p char) ; 拡張ASCIIも認める
		   (write-char char s))
		  (t (unread-char char *pdf-input-stream*)
		     (return)))))))

(defun extended-ascii-p (char)
  (<= 128 (char-code char) 253))

(defmethod write-object ((obj string) &optional root-level)
  (declare (ignorable root-level))
  #+(and lispworks pdf-binary)
  (if (lw:text-string-p obj)		; may include unicode characters
      (loop for char across obj
            if (lw:base-char-p char)
            do (write-char char *pdf-stream*)
            else do (write-byte (char-external-code char *default-charset*) *pdf-stream*))
      (write-string obj *pdf-stream*))
  #-(and lispworks pdf-binary)
  (if (char= #\/ (aref obj 0))          ; When obj is a NAME
      (loop for char across obj
	 if (or (whitespace-p char)     ; 空白が含まれる名前の場合whitespaceを其の儘出すのは不味い
		(extended-ascii-p char))
	 do
	   (format *pdf-stream* "#~2,'0x" (char-code char))
	 else do
              (write-char char *pdf-stream*))
      ;; When obj is a STRING
      (write-string obj *pdf-stream*)))

(defun read-object (&optional (eof-error-p t))
  "Returns one of the following PDF objects: boolean (:true or :false),
   number (Lisp number), string (Lisp string), name (Lisp symbol in the PDF
   package), array (Lisp vector), dictionary (Lisp property list), stream
   (Lisp pdf-stream) or null (Lisp NIL). When EOF-ERRORP is nil, it returns
   :eof for the end of the stream (otherwise it signals an error)." 
  (skip-whitespace eof-error-p)
  (let ((char (peek-char nil *pdf-input-stream* eof-error-p)))
    (cond ((numeric-char-p char)
           (read-number))
          ((eql char #\()
           (eat-char #\()
           (read-pdf-string))
          ((eql char #\/)
           (eat-char #\/)
           (read-name ))
          ((eql char #\[)
           (eat-char #\[)
           (read-array))
          ((eql char #\<) 
           (eat-char #\<)
           (let ((next-char (peek-char nil *pdf-input-stream*)))
             (if (char= next-char #\<) 
                 (progn (eat-char #\<)
                   (read-dictionary-or-stream))
               (read-hex-string))))
          #+ignore                      ; This can't be happened.
          ((eql char #\#)
           (eat-char #\#)
           (with-output-to-string (s)
             (let ((digit-1 (read-char *pdf-input-stream*))
                   (digit-2 (read-char *pdf-input-stream*)))
               (unless (and (hex-digit-char-p digit-1)
                            (hex-digit-char-p digit-2))
                 (error 'pdf-parse-error
                        :stream *pdf-input-stream*
                        :message "Illegal hexadecimal escape sequence in PDF name."))
               (write-char (code-char (parse-hex digit-1 digit-2)) s))))
          ((eql char #\t)
           (eat-chars "true")
           "true")
          ((eql char #\f)
           (eat-chars "false")
           "false")
          ((eql char #\n)
           (eat-chars "null")
           nil)
          ((eql char #\e)
           ;; this is probably an empty indirect object.  WRITE-OBJECT
           ;; can write them, so we should be able to read them too.
           nil)
          ((or (eql char #\R) (eql char #\U))
           ;; Add this clause to avoid parsing error
           ;; when it encouters "UNREAD" or "R" line. -- by H.Kuroda
           nil)
          (t (unexpected-character-error char)))))

(defclass pseudo-font-object (object-ref)
  ((name :accessor name :initform (gen-name "/CLF") :initarg :name)))

(defun find-font-object (font &key (embed *embed-fonts*))
  (let ((font-object (cdr (assoc font (fonts *document*)))))
    (unless font-object
      (let* ((font-name (format nil "/CLF~A" (name font)))
             (font-indirect-object (find-font-resources font-name)))
        ;; pdfの肥大化を防ぐため、(以下にreportが肥大化する樣をmemoしてある)
        ;; existing-documentの中に同じ名前のfont指定がすでになされてゐたらfont-objectを作らない
        ;; 換りにpseudo-font-objectを作る
        (if (null font-indirect-object)
            (progn
              (setf font-object (make-instance 'font-object
                                  :font font
                                  ;; font毎に個別の名前を與へるやうにしておかないと、
                                  ;; 一旦fileを閉ぢたあと、改めて別のfontを指定しようとしたとき、
                                  ;; 名前が衝突してしまふ
                                  :name font-name :embed embed))
              (push (cons font font-object) (fonts *document*)))
          (setf font-object (make-instance 'pseudo-font-object
                              :obj-number (obj-number font-indirect-object)
                              :gen-number (gen-number font-indirect-object)
                              :name font-name)))))
    font-object))

(defun add-font-to-page (font &key (embed *embed-fonts*))
  ;; embed :default ではなく、*embed-fonts* に從ふやうにした
  (let ((font-object (cdr (assoc font (fonts *page*)))))
    (unless font-object
      (setf font-object (find-font-object font :embed embed))
      (push (cons font font-object) (fonts *page*))
      ;; 重複を防ぐために add-dict-value ではなくて change-dict-value
      (change-dict-value (font-objects *page*) (name font-object) font-object))
    font-object))


(defmethod initialize-instance :after ((obj pdf-stream) &key empty &allow-other-keys)
  (unless empty
    (change-dict-value obj "/Length"    ; To avoid duplicate /Length
                       #'(lambda ()
                           (let ((content (content obj)))
                             (if (consp content)
                                 (reduce #'+ content :key #'length)
                               (length content)))))))

;;;
;;; Follows are New Definitions by H.Kuroda
;;;

(defun change-page-dict-value (page key new-value)
  "Look KEY up in the page dictionary.  If it is not found, look it up
in the parent page dictionary, and change its original value with new-value."
  ;; Some values in a page dictionary can be inherited from the parent
  ;; page's dictionary, such as the MediaBox.  This is a handy way to
  ;; look them up.  See PDF-REF Table 3.18.
  (let ((value (resolve-dict-value page key)))
    (if value
        (change-dict-value page key new-value)
      (let ((parent-page (resolve-dict-value page "/Parent")))
        (when parent-page
          (change-page-dict-value parent-page key new-value))))))

(defun find-font-resources (fontname)
  (let* ((resources (get-dict-value (content *page*) "/Resources"))
         (fonts (get-dict-value resources "/Font")))
    (loop for (name . object) in (dict-values fonts)
        when (string= fontname name)
        return object)))


;;;
;;; image には固有の名前が必要
;;;
(defmethod make-image :around (object &key name &allow-other-keys)
  (declare (ignore object))
  (let ((obj (call-next-method)))
    (when name
      (setf (name obj) name))
    obj))


;;;;
;;;; 以下試驗中 (unite pdf)
;;;;

#+ignore
(defun copy-page-object (page)
  (let* ((src-page page)
	 (src-dict (content src-page))
	 (resources (ensure-dictionary (get-dict-value src-dict "/Resources")))
	 (fonts (ensure-dictionary (get-dict-value resources "/Font")))
	 (xobjects (ensure-dictionary (get-dict-value resources "/XObject")))
	 (new-page (make-instance 'page))
	 (new-dict (setf (content new-page) (copy-dict src-dict)))
         (content-stream (make-instance 'pdf-stream)))
    (setf *original-content* (copy-pdf-structure (get-dict-value src-dict "/Contents")))
    (setf *current-content* (make-array 10 :fill-pointer 0 :adjustable t))
    (unless resources
      (setf resources (make-instance 'dictionary)))
    (change-dict-value new-dict "/Resources" resources)
    (unless fonts
      (setf fonts (make-instance 'dictionary)))
    (change-dict-value resources "/Font" fonts)
    (unless xobjects
      (setf xobjects (make-instance 'dictionary)))
    (change-dict-value resources "/XObject" xobjects)
    (setf (bounds new-page) (get-dict-value src-dict "/MediaBox")
	  (resources new-page) resources
	  (font-objects new-page) fonts
	  (xobjects new-page) xobjects
	  (content-stream new-page) content-stream)
    (change-dict-value new-dict "/Contents" *current-content*)
    new-page))

#+ignore
(defmacro with-copy-page ((page) &body body)
  `(let* ((*page* (copy-page-object ,page)))
    (with-standard-io-syntax
	(setf (content (content-stream *page*))
	 (with-output-to-string (*page-stream*)
	   ,@body)))
     t))

#+ignore
(defun collect-documents (file)
  (with-open-file (*pdf-input-stream* file
                   :direction :input
                   :external-format +external-format+)
    (let ((xref-list (reverse (find-cross-reference-start-list))))
      (loop for xref in xref-list
          collect
            (let ((*indirect-objects* (make-hash-table :test #'equal))
                  (*document* (make-instance 'document :empty t)))
              (read-pdf-xref xref)
              *document*)))))

#+ignore
(defun read-pdf-xref (xref)
  (let* ((trailer (read-xref-and-trailer xref)))
    (setf (catalog *document*) (get-dict-value trailer "/Root")
          (docinfo *document*) (get-dict-value trailer "/Info"))
    (load-all-indirect-objects)
    (let* ((root-page-node (change-class (get-dict-value (content (catalog *document*)) "/Pages") 'page-node))
	   (pages-vector (make-array 1 :fill-pointer 0 :adjustable t)))
      (collect-pages root-page-node pages-vector root-page-node)
      (setf (pages root-page-node) pages-vector)
      (change-dict-value (content root-page-node) "/Count" #'(lambda () (length (pages root-page-node))))
      (change-dict-value (content root-page-node) "/Kids" (pages root-page-node))
      (renumber-all-indirect-objects)
      (setf (root-page *document*) root-page-node))))

#+ignore
(defun canonicalize-pdf (in-file out-file)
  (let* ((documents (collect-documents in-file)))
    (with-document ()
      ;; (setf (catalog *document*) (catalog (first documents))
      ;;       (docinfo *document*) (docinfo (first documents)))
      (loop for document in documents
          for old-root = (root-page document)
          for old-pages = (pages old-root)
          do (loop for obj across (objects document)
                 do (vector-push-extend obj (objects *document*)))
             (loop for page across old-pages
                 do (with-copy-page (page)
                      (insert-original-page-content))))
      ;; renumber all indirect objects
      (loop for object across (objects *document*)
          for number from 1 do
            (setf (obj-number object) number))
      (write-document out-file))))

#+ignore
(defun find-cross-reference-start-list ()
  (let ((buffer (make-string +xref-search-size+)))
    (loop with keyword = "startxref"
        with keyword-length = (length keyword)
        for n = (read-sequence buffer *pdf-input-stream*)
        while (> n 0) append
          (let ((position (search keyword buffer)))
            (if position
                (list (parse-integer buffer :start (+ position 10) :junk-allowed t))
              (prog1 nil
                (file-position *pdf-input-stream*
                               (- (file-position *pdf-input-stream*) keyword-length))))))))
#+ignore
(defun make-template-list-from-page (old-page &key scale rotate
                                                   (translate-x 0) (translate-y 0))
  (when (typep old-page 'indirect-object)
    (setf old-page (content old-page)))
  
  (let* ((old-content (resolve-dict-value old-page "/Contents"))
         (old-content-stream-vector (if (vectorp old-content)
                                        (map 'vector #'content old-content)
                                      (vector old-content))))
                                        ; resolve-dict-value から vector が返されることがある
    (loop for old-content-stream across old-content-stream-vector
        collect
          (let* ((template (make-instance 'template :bounds
                                          (or (resolve-page-dict-value old-page "/MediaBox")
                                              *default-template-size*)))
                 (new-content-stream (content template)))
            ;; copy the old content stream 
            (setf (content new-content-stream)
              (content old-content-stream))
            ;; copy the other dict entries, e.g. filter, length, from the old stream
            (loop for ((name . value)) on (dict-values old-content-stream) do
                  (change-dict-value new-content-stream name (copy-pdf-structure value)))
            (let ((form-matrix +identity-matrix+))
              ;; rotate the template
              (when (and rotate (not (zerop rotate)))        
                (setf form-matrix
                  (let ((translate-to-origin (translation-matrix
                                              (- (/ (template-width template) 2))
                                              (- (/ (template-height template) 2))))
                        (translate-back (translation-matrix
                                         (/ (template-width template) 2)
                                         (/ (template-height template) 2))))
                    (multiply-tranformation-matrices
                     (multiply-tranformation-matrices translate-to-origin
                                                      (rotation-matrix rotate))
                     translate-back))))
              ;; scale the template
              (when scale
                (setf form-matrix
                  (multiply-tranformation-matrices
                   form-matrix
                   (scale-matrix scale scale))))

              ;;translate the template
              (setf form-matrix
                (multiply-tranformation-matrices
                 form-matrix
                 (translation-matrix translate-x translate-y)))

              ;; set the template's form matrix, see PDF-REF Table 4.41
              (change-dict-value (content template) "/Matrix" form-matrix))

            ;; copy any metadata associated with the page.  Not sure if this
            ;; is what we want.
            (when (get-dict-value old-page "/Metadata")
              (add-dict-value (content template) "/Metadata"
                              (copy-pdf-structure (get-dict-value old-page "/Metadata"))))

            ;; copy the page's resources, such as images and fonts.
            (let ((old-resources (resolve-page-dict-value old-page "/Resources")))
              (loop for ((name . value)) on (dict-values old-resources) do
                    (change-dict-value (resources template) name (copy-pdf-structure value))))
            template))))
